package dk.rm.akutplads.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import dk.digitalidentity.saml.model.TokenUser;

@Component
public class SecurityUtil {

	@Value("${saml.attribute.cvr}")
	private String cvrAttribute;

	@Value("${saml.attribute.name}")
	private String nameAttribute;

	public String getCvr() {
		return getAttribute(cvrAttribute);
	}

	public String getName() {
		return getAttribute(nameAttribute);
	}

	public String getUsername() {
		if (isLoggedIn()) {
			return (String) ((TokenUser) SecurityContextHolder.getContext().getAuthentication().getDetails()).getUsername();
		}
		return null;
	}

	public boolean isMunicipalityUser() {
		return hasRole("ROLE_MUNICIPALITY");
	}

	public boolean isVisitatorUser() {
		return hasRole("ROLE_VISITATOR");
	}

	public boolean isEmergencyDoctorUser() {
		return hasRole("ROLE_EMERGENCYDOCTOR");
	}
	
	public boolean isAdmin() {
		return hasRole("ROLE_ADMIN");
	}

	private String getAttribute(String attribute) {
		if (isLoggedIn()) {
			return (String) ((TokenUser) SecurityContextHolder.getContext().getAuthentication().getDetails()).getAttributes().get(attribute);
		}

		return null;
	}

	private boolean hasRole(String role) {
		if (isLoggedIn()) {
			for (GrantedAuthority grantedAuthority : (SecurityContextHolder.getContext().getAuthentication()).getAuthorities()) {
				if (role.equals(grantedAuthority.getAuthority())) {
					return true;
				}
			}
		}

		return false;
	}

	private static boolean isLoggedIn() {
		if (SecurityContextHolder.getContext().getAuthentication() != null && SecurityContextHolder.getContext().getAuthentication().getDetails() != null
				&& SecurityContextHolder.getContext().getAuthentication().getDetails() instanceof TokenUser) {
			return true;
		}

		return false;
	}

}
