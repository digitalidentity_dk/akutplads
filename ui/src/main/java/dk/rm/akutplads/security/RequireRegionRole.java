package dk.rm.akutplads.security;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import org.springframework.security.access.prepost.PreAuthorize;

@Retention(RetentionPolicy.RUNTIME)
@PreAuthorize("hasRole('ROLE_EMERGENCYDOCTOR') || hasRole('ROLE_VISITATOR')")
public @interface RequireRegionRole {

}
