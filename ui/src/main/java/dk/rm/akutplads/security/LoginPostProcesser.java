package dk.rm.akutplads.security;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import dk.digitalidentity.saml.extension.SamlLoginPostProcessor;
import dk.digitalidentity.saml.model.TokenUser;
import dk.rm.akutplads.dao.UserDao;
import dk.rm.akutplads.dao.model.User;

@Component
public class LoginPostProcesser implements SamlLoginPostProcessor {

	@Value("${saml.attribute.usertype}")
	private String usertypeAttribute;
	
	@Value("${saml.attribute.usertype.municipalityvalue}")
	private String usertypeAttributeMunicipalityValue;
	
	@Value("${saml.attribute.usertype.visitatorvalue}")
	private String usertypeAttributeVisitatorValue;
	
	@Value("${saml.attribute.usertype.emergencydoctorvalue}")
	private String usertypeAttributeEmergencyDoctorValue;
	
	@Value("${saml.attribute.usertype.adminvalue}")
	private String usertypeAttributeAdminValue;

	@Autowired
	private UserDao userDao;

	@Override
	public void process(TokenUser tokenUser) {
		String userType = (String) tokenUser.getAttributes().get(usertypeAttribute);
		
		User user = userDao.findByUserId(tokenUser.getUsername());
		if (user == null) {
			user = new User();
			user.setUserId(tokenUser.getUsername());
			user.setSeenSplashScreen(false);
			
			userDao.save(user);
		}

		Set<GrantedAuthority> authorities = new HashSet<>();

		if (usertypeAttributeVisitatorValue.equals(userType)) {
			authorities.add(new SimpleGrantedAuthority("ROLE_VISITATOR"));
		}
		else if (usertypeAttributeEmergencyDoctorValue.equals(userType)) {
			authorities.add(new SimpleGrantedAuthority("ROLE_EMERGENCYDOCTOR"));
		}
		else if (usertypeAttributeMunicipalityValue.equals(userType)) {
			authorities.add(new SimpleGrantedAuthority("ROLE_MUNICIPALITY"));
		}
		else if (usertypeAttributeAdminValue.equals(userType)) {
			authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));	
		}
		else {
			throw new UsernameNotFoundException("Unknown usertype: " + userType);
		}
		
		tokenUser.setAuthorities(authorities);
	}
}
