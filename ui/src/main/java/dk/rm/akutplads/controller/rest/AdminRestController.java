package dk.rm.akutplads.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import dk.rm.akutplads.dao.ServiceDao;
import dk.rm.akutplads.dao.model.Service;
import dk.rm.akutplads.security.RequireAdminRole;

@RestController
@RequireAdminRole
public class AdminRestController {

	@Autowired
	private ServiceDao serviceDao;

	@DeleteMapping(value = { "/rest/admin/delete/{id}" })
	@ResponseBody
	public HttpEntity<String> delete(Model model, @PathVariable("id") long id) {
		Service service = serviceDao.getOne(id);
		if (service == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		serviceDao.delete(service);

		return new ResponseEntity<>(HttpStatus.OK);
	}
}
