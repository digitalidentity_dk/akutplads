package dk.rm.akutplads.controller.mvc;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import dk.rm.akutplads.dao.MunicipalityDao;
import dk.rm.akutplads.dao.ServiceDao;
import dk.rm.akutplads.dao.model.Municipality;
import dk.rm.akutplads.dao.model.Service;
import dk.rm.akutplads.security.RequireRegionRole;
import dk.rm.akutplads.security.SecurityUtil;

@Controller
@RequireRegionRole
public class RegionController {

	@Autowired
	private MunicipalityDao municipalityDao;

	@Autowired
	private ServiceDao serviceDao;

	@Autowired
	private SecurityUtil securityUtil;

	@GetMapping("/region/gps")
	public String regionGps(Model model) {
		return "region/gps";
	}

	@GetMapping("/region/search")
	public String regionSearch(Model model, @RequestParam(name = "q", required = false, defaultValue = "") String query, HttpServletRequest request) {
		if (query == null || query.length() == 0) {
			query = (String) request.getSession().getAttribute("query");
		}

		model.addAttribute("allMunicipalities", municipalityDao.findAll());
		model.addAttribute("query", query);

		return "region/search";
	}

	@GetMapping("/region/services/{id}")
	public String regionServices(Model model, @PathVariable("id") Long mId, HttpServletRequest request) {
		Optional<Municipality> m = municipalityDao.findById(mId);
		if (!m.isPresent()) {
			return "region/nomunicipality";
		}

		List<Service> allServices = null;
		if (securityUtil.isMunicipalityUser()) {
			allServices = serviceDao.findAll();
		}
		else if (securityUtil.isVisitatorUser()) {
			allServices = serviceDao.findByVisibleForVisitatorTrue();
		}
		else if (securityUtil.isEmergencyDoctorUser()) {
			allServices = serviceDao.findByVisibleForEmergencyDoctorTrue();
		}
		
		allServices.sort(new Comparator<Service>() {
			@Override
			public int compare(Service s1, Service s2) {
				return s1.getName().compareTo(s2.getName());
			}
		});
		
		model.addAttribute("allServices", allServices);
		model.addAttribute("municipality", m.get());
		model.addAttribute("isVisitator", securityUtil.isVisitatorUser());
		model.addAttribute("backUrl", request.getContextPath() + "/region/search");

		return "region/services";
	}

	@GetMapping("/region/municipality/{municipalityCode}")
	public String regionList(Model model, @PathVariable("municipalityCode") String municipalityCode, @RequestParam(name = "q", required = false, defaultValue = "") String query, HttpServletRequest request) {
		if (query != null && query.length() > 0) {
			request.getSession().setAttribute("query", query);
		}

		Municipality m = municipalityDao.findByMunicipalityCode(municipalityCode);
		if (m == null) {
			return "region/nomunicipality";
		}

		return "redirect:/region/services/" + m.getId();
	}
}
