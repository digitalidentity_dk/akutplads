package dk.rm.akutplads.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import dk.rm.akutplads.controller.rest.model.TextDTO;
import dk.rm.akutplads.dao.MunicipalityDao;
import dk.rm.akutplads.dao.ServiceDao;
import dk.rm.akutplads.dao.model.Municipality;
import dk.rm.akutplads.dao.model.MunicipalityService;
import dk.rm.akutplads.dao.model.Service;
import dk.rm.akutplads.security.RequireMunicipalityRole;
import dk.rm.akutplads.security.SecurityUtil;

@RestController
@RequireMunicipalityRole
public class MunicipalityRestController {

	@Autowired
	private ServiceDao serviceDao;

	@Autowired
	private SecurityUtil securityUtil;

	@Autowired
	private MunicipalityDao municipalityDao;

	@GetMapping(value = { "/rest/municipality/service/{serviceId}" })
	@ResponseBody
	public HttpEntity<String> updateService(Model model, @PathVariable("serviceId") long serviceId, @RequestHeader("remove") boolean remove) {
		Municipality m = municipalityDao.findByCvr(securityUtil.getCvr());
		if (m == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		if (remove) {
			m.getServices().removeIf(s -> s.getService().getId() == serviceId);
		}
		else {
			Service service = serviceDao.findById(serviceId);
			if (service != null) {
				MunicipalityService municipalityService = new MunicipalityService();
				municipalityService.setService(service);
				municipalityService.setMunicipality(m);

				m.getServices().add(municipalityService);
			}
		}

		municipalityDao.save(m);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@GetMapping(value = { "/rest/municipality/service/homenurse" })
	@ResponseBody
	public HttpEntity<String> updateServiceHomeNurse(Model model, @RequestHeader("homenurse") boolean homeNurse) {
		Municipality m = municipalityDao.findByCvr(securityUtil.getCvr());
		m.setHomeNurse(homeNurse);
		municipalityDao.save(m);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@GetMapping(value = { "/rest/municipality/service/beds" })
	@ResponseBody
	public HttpEntity<String> updateBeds(Model model, @RequestHeader("beds") Integer beds) {
		Municipality m = municipalityDao.findByCvr(securityUtil.getCvr());
		m.setBeds(beds);
		municipalityDao.save(m);

		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@PostMapping(value = { "/rest/municipality/service/{id}/notes" })
	@ResponseBody
	public HttpEntity<String> updateServiceNotes(Model model, @PathVariable("id") long id, @RequestBody TextDTO text) {
		Municipality m = municipalityDao.findByCvr(securityUtil.getCvr());
		Service service = serviceDao.findById(id);

		if (m == null || service == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		for (MunicipalityService municipalityService : m.getServices()) {
			if (municipalityService.getService().getId() == service.getId()) {
				municipalityService.setNotes(text.getText());
				serviceDao.save(service);
				
				break;
			}
		}
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
