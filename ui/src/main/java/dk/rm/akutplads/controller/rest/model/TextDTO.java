package dk.rm.akutplads.controller.rest.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TextDTO {
	private String text;
}
