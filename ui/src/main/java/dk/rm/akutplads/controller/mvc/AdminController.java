package dk.rm.akutplads.controller.mvc;

import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import dk.rm.akutplads.dao.ServiceDao;
import dk.rm.akutplads.dao.model.Service;
import dk.rm.akutplads.security.RequireAdminRole;

@Controller
@RequireAdminRole
public class AdminController {

	@Autowired
	private ServiceDao serviceDao;

	@GetMapping("/admin/manageservices")
	public String list(Model model) {
		List<Service> allServices = serviceDao.findAll();
		allServices.sort(new Comparator<Service>() {

			@Override
			public int compare(Service s1, Service s2) {
				return s1.getName().compareTo(s2.getName());
			}
		});

		Service newService = new Service();
		newService.setVisibleForEmergencyDoctor(true);
		newService.setVisibleForVisitator(true);

		model.addAttribute("services", allServices);
		model.addAttribute("newService", newService);

		return "admin/manageservices";
	}
	
	@PostMapping("/admin/addService")
	public String add(Model model, @ModelAttribute Service newService) {
		if (newService.getName() != null && newService.getName().length() > 0) {
			Service service = new Service();
			service.setName(newService.getName());
			service.setVisibleForVisitator(newService.isVisibleForVisitator());
			service.setVisibleForEmergencyDoctor(newService.isVisibleForEmergencyDoctor());
			
			serviceDao.save(service);
		}

		return "redirect:/admin/manageservices";
	}
	
	@PostMapping("/admin/editService")
	public String edit(Model model, @ModelAttribute Service existingService) {
		if (existingService.getName() != null && existingService.getName().length() > 0) {
			Service service = serviceDao.findById(existingService.getId());
			if (service != null) {
				service.setName(existingService.getName());
				service.setVisibleForVisitator(existingService.isVisibleForVisitator());
				service.setVisibleForEmergencyDoctor(existingService.isVisibleForEmergencyDoctor());
				
				serviceDao.save(service);				
			}
		}

		return "redirect:/admin/manageservices";
	}
}
