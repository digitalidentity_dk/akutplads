package dk.rm.akutplads.controller.mvc;

import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import dk.rm.akutplads.dao.MunicipalityDao;
import dk.rm.akutplads.dao.ServiceDao;
import dk.rm.akutplads.dao.UserDao;
import dk.rm.akutplads.dao.model.Municipality;
import dk.rm.akutplads.dao.model.Service;
import dk.rm.akutplads.dao.model.User;
import dk.rm.akutplads.security.RequireMunicipalityRole;
import dk.rm.akutplads.security.SecurityUtil;

@Controller
@RequireMunicipalityRole
public class MunicipalityController {
	private static String[] times = {
		"00:00", "00:15", "00:30", "00:45",
		"01:00", "01:15", "01:30", "01:45",
		"02:00", "02:15", "02:30", "02:45",
		"03:00", "03:15", "03:30", "03:45",
		"04:00", "04:15", "04:30", "04:45",
		"05:00", "05:15", "05:30", "05:45",
		"06:00", "06:15", "06:30", "06:45",
		"07:00", "07:15", "07:30", "07:45",
		"08:00", "08:15", "08:30", "08:45",
		"09:00", "09:15", "09:30", "09:45",
		"10:00", "10:15", "10:30", "10:45",
		"11:00", "11:15", "11:30", "11:45",
		"12:00", "12:15", "12:30", "12:45",
		"13:00", "13:15", "13:30", "13:45",
		"14:00", "14:15", "14:30", "14:45",
		"15:00", "15:15", "15:30", "15:45",
		"16:00", "16:15", "16:30", "16:45",
		"17:00", "17:15", "17:30", "17:45",
		"18:00", "18:15", "18:30", "18:45",
		"19:00", "19:15", "19:30", "19:45",
		"20:00", "20:15", "20:30", "20:45",
		"21:00", "21:15", "21:30", "21:45",
		"22:00", "22:15", "22:30", "22:45",
		"23:00", "23:15", "23:30", "23:45"
	};
	
	@Autowired
	private MunicipalityDao municipalityDao;

	@Autowired
	private ServiceDao serviceDao;

	@Autowired
	private SecurityUtil securityUtil;

	@Autowired
	private UserDao userDao;

	@GetMapping("/municipality/services")
	public String kommuneServices(Model model) {
		Municipality m = municipalityDao.findByCvr(securityUtil.getCvr());
		if (m == null) {
			return "municipality/nomunicipality";
		}

		User user = userDao.findByUserId(securityUtil.getUsername());
		if (user != null && !user.isSeenSplashScreen()) {
			model.addAttribute("showSplashScreen", true);
			user.setSeenSplashScreen(true);
			userDao.save(user);
		}
		else {
			model.addAttribute("showSplashScreen", false);
		}

		List<Service> allServices = serviceDao.findAll();
		allServices.sort(new Comparator<Service>() {
			@Override
			public int compare(Service s1, Service s2) {
				return s1.getName().compareTo(s2.getName());
			}
		});
		
		model.addAttribute("allServices", allServices);
		model.addAttribute("municipality", m);

		return "municipality/services";
	}

	@GetMapping("/municipality/contact")
	public String kommuneContact(Model model) {
		model.addAttribute("municipality", municipalityDao.findByCvr(securityUtil.getCvr()));
		model.addAttribute("times", times);
		
		return "municipality/contact";
	}

	@PostMapping("/municipality/contact")
	public String kommuneContact(Model model, Municipality municipality) {
		Municipality m = municipalityDao.findByCvr(securityUtil.getCvr());
		if (m == null) {
			return "municipality/nomunicipality";
		}

		m.setPhoneNumber(municipality.getPhoneNumber());
		m.setPhoneNumberSecondary(municipality.getPhoneNumberSecondary());
		if (municipality.getPhoneNumberFrom().compareTo(municipality.getPhoneNumberTo()) <= 0) {
			m.setPhoneNumberFrom(municipality.getPhoneNumberFrom());
			m.setPhoneNumberTo(municipality.getPhoneNumberTo());
		}
		municipalityDao.save(m);

		model.addAttribute("showNotify", true);
		model.addAttribute("municipality", m);
		model.addAttribute("times", times);

		return "municipality/contact";
	}
}
