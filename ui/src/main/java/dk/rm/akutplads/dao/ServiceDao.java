package dk.rm.akutplads.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import dk.rm.akutplads.dao.model.Service;

public interface ServiceDao extends JpaRepository<Service, Long> {
	Service findById(long id);

	List<Service> findByVisibleForVisitatorTrue();

	List<Service> findByVisibleForEmergencyDoctorTrue();
}
