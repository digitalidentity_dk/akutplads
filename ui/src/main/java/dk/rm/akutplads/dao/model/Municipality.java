package dk.rm.akutplads.dao.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Municipality {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column
	private String name;

	@Column
	private String cvr;

	@Column
	private String municipalityCode;

	@Column
	private String phoneNumber;
	
	@Column
	private String phoneNumberFrom;

	@Column
	private String phoneNumberTo;

	@Column
	private String phoneNumberSecondary;

	@Column
	private Integer beds;

	@Column
	private Boolean homeNurse;

	@OneToMany(mappedBy = "municipality", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<MunicipalityService> services;
	
	@Transient
	public List<Service> getServicesUnwrapped() {
		if (services != null) {
			return services.stream().map(s -> s.getService()).collect(Collectors.toList());
		}
		
		return new ArrayList<>();
	}

	@Transient
	public String getPhoneNumberTrimmed() {
		String phone = currentPhone();
		
		if (phone != null) {
			return phone.replace(" ", "");
		}
		
		return null;
	}
	
	@Transient
	public String getNotes(long id) {
		for (MunicipalityService service : services) {
			if (service.getService().getId() == id) {
				return service.getNotes();
			}
		}
		
		return null;
	}
	
	@Transient
	public String getPhoneNumberSpaced() {
		String phone = currentPhone();

		if (phone != null) {
			StringBuilder builder = new StringBuilder();
			String trimmedPhoneNumber = phone.replace(" ", "");
			int offSet = 0;

			if (trimmedPhoneNumber.length() >= 3 && trimmedPhoneNumber.startsWith("+")) {
				builder.append(trimmedPhoneNumber.subSequence(0, 3));
				offSet = 3;
			}
			
			while (trimmedPhoneNumber.length() >= (offSet + 2)) {
				if (builder.length() > 0) {
					builder.append(" ");
				}
				builder.append(trimmedPhoneNumber.subSequence(offSet, offSet + 2));
				offSet += 2;
			}			
			
			return builder.toString();
		}
		
		return null;
	}
	
	private String currentPhone() {
		Calendar calendar = GregorianCalendar.getInstance();
		int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
		int currentMinute = calendar.get(Calendar.MINUTE);

		String phoneFrom = (phoneNumberFrom != null && phoneNumberFrom.length() == 5) ? phoneNumberFrom : "00:00";
		String phoneTo = (phoneNumberTo != null && phoneNumberTo.length() == 5) ? phoneNumberTo : "23:45";

		int hourFrom = getHour(phoneFrom);
		int minuteFrom = getMinute(phoneFrom);
		
		int hourTo = getHour(phoneTo);
		int minuteTo = getMinute(phoneFrom);
		
		if ((currentHour > hourFrom && currentHour < hourTo) ||
			(currentHour == hourFrom && currentMinute >= minuteFrom) ||
			(currentHour == hourTo && currentMinute < minuteTo)) {

			return phoneNumber;
		}

		return phoneNumberSecondary;
	}
	
	private int getHour(String value) {
		return Integer.parseInt(value.substring(0, 2));
	}

	private int getMinute(String value) {
		return Integer.parseInt(value.substring(3));
	}
}
