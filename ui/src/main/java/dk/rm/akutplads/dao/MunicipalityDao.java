package dk.rm.akutplads.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import dk.rm.akutplads.dao.model.Municipality;

public interface MunicipalityDao extends JpaRepository<Municipality, Long> {
	Municipality findByCvr(String cvr);
	Municipality findByMunicipalityCode(String code);
}
