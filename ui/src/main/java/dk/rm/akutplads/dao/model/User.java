package dk.rm.akutplads.dao.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class User {
	
	@Id
	private long id;

	@Column
	private String userId;

	@Column
	private boolean seenSplashScreen;
}
