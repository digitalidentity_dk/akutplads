package dk.rm.akutplads.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import dk.rm.akutplads.dao.model.User;

public interface UserDao extends JpaRepository<User, Long> {

	User findByUserId(String userId);

}
