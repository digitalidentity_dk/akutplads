CREATE TABLE municipality (
    id                            BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name                          VARCHAR(64) NOT NULL,
    cvr                           VARCHAR(8) NOT NULL,
    municipality_code             VARCHAR(4) NOT NULL,
    phone_number                  VARCHAR(255),
    beds                          INT,
    home_nurse                    BOOLEAN NOT NULL DEFAULT 0
);

CREATE TABLE service (
    id                            BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name                          VARCHAR(255) NOT NULL,
    description                   TEXT,
    visible_for_visitator         BOOLEAN NOT NULL DEFAULT 0,
    visible_for_emergency_doctor  BOOLEAN NOT NULL DEFAULT 0
);

CREATE TABLE municipality_service (
    municipality_id               BIGINT NOT NULL,
    service_id                    BIGINT NOT NULL,

    FOREIGN KEY (municipality_id) REFERENCES municipality(id),
    FOREIGN KEY (service_id) REFERENCES service(id)
);
