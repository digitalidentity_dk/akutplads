CREATE TEMPORARY TABLE tmp_municipality_service LIKE municipality_service; 
INSERT INTO tmp_municipality_service SELECT * FROM municipality_service;

DROP TABLE municipality_service;

CREATE TABLE municipality_service (
	id                            BIGINT PRIMARY KEY AUTO_INCREMENT,
    municipality_id               BIGINT NOT NULL,
    service_id                    BIGINT NOT NULL,
    notes                         TEXT,

    FOREIGN KEY (municipality_id) REFERENCES municipality(id) ON DELETE CASCADE,
    FOREIGN KEY (service_id) REFERENCES service(id) ON DELETE CASCADE
);

INSERT INTO municipality_service (municipality_id, service_id) 
  SELECT municipality_id, service_id
  FROM tmp_municipality_service;
