INSERT INTO municipality (name, cvr, municipality_code) VALUES ('Skanderborg Kommune', '29189633', '0746');
INSERT INTO municipality (name, cvr, municipality_code) VALUES ('Favrskov Kommune', '29189714', '0710');

INSERT INTO service (name, description, visible_for_visitator, visible_for_emergency_doctor) VALUES ("Akutte blodprøver (blodsukker, hæmoglobin, CRP og Leucodiff)", "beskrivelse...", 1, 1);
INSERT INTO service (name, description, visible_for_visitator, visible_for_emergency_doctor) VALUES ("Genanlæggelse af Peg-sonde", "beskrivelse...", 1, 1);
INSERT INTO service (name, description, visible_for_visitator, visible_for_emergency_doctor) VALUES ("Subkutan væske", "beskrivelse...", 1, 1);
INSERT INTO service (name, description, visible_for_visitator, visible_for_emergency_doctor) VALUES ("Sat-måling", "beskrivelse...", 1, 1);

INSERT INTO service (name, description, visible_for_visitator, visible_for_emergency_doctor) VALUES ("Akutte blodprøver (INR)", "beskrivelse...", 0, 1);
INSERT INTO service (name, description, visible_for_visitator, visible_for_emergency_doctor) VALUES ("Genanlæggelse af subra pub.kath.", "beskrivelse...", 0, 1);

INSERT INTO service (name, description, visible_for_visitator, visible_for_emergency_doctor) VALUES ("Blærescanning", "beskrivelse...", 1, 0);
INSERT INTO service (name, description, visible_for_visitator, visible_for_emergency_doctor) VALUES ("Anlæggelse af blære kateter", "beskrivelse...", 1, 0);
INSERT INTO service (name, description, visible_for_visitator, visible_for_emergency_doctor) VALUES ("Anlæggelse af top kateter", "beskrivelse...", 1, 0);
INSERT INTO service (name, description, visible_for_visitator, visible_for_emergency_doctor) VALUES ("Inhalationer med forstøverapparat", "beskrivelse...", 1, 0);
INSERT INTO service (name, description, visible_for_visitator, visible_for_emergency_doctor) VALUES ("Iv adgang ( anlæggelse og skift)", "beskrivelse...", 1, 0);
INSERT INTO service (name, description, visible_for_visitator, visible_for_emergency_doctor) VALUES ("Iv antibiotika (indgift)", "beskrivelse...", 1, 0);
INSERT INTO service (name, description, visible_for_visitator, visible_for_emergency_doctor) VALUES ("Iv væske", "beskrivelse...", 1, 0);
INSERT INTO service (name, description, visible_for_visitator, visible_for_emergency_doctor) VALUES ("Parenteral ernæring", "beskrivelse...", 1, 0);
INSERT INTO service (name, description, visible_for_visitator, visible_for_emergency_doctor) VALUES ("Sonde (anlæggelse og skift af nasalsonder)", "beskrivelse...", 1, 0);
INSERT INTO service (name, description, visible_for_visitator, visible_for_emergency_doctor) VALUES ("Palliation / akutkasse", "beskrivelse...", 1, 0);
INSERT INTO service (name, description, visible_for_visitator, visible_for_emergency_doctor) VALUES ("Pleuradrænage", "beskrivelse...", 1, 0);
INSERT INTO service (name, description, visible_for_visitator, visible_for_emergency_doctor) VALUES ("Seponering af kemopumper", "beskrivelse...", 1, 0);
INSERT INTO service (name, description, visible_for_visitator, visible_for_emergency_doctor) VALUES ("St.p. af lunger", "beskrivelse...", 1, 0);
INSERT INTO service (name, description, visible_for_visitator, visible_for_emergency_doctor) VALUES ("Urin stix", "beskrivelse...", 1, 0);
INSERT INTO service (name, description, visible_for_visitator, visible_for_emergency_doctor) VALUES ("Re etablering af vaccumbehandling af sår", "beskrivelse...", 1, 0);
INSERT INTO service (name, description, visible_for_visitator, visible_for_emergency_doctor) VALUES ("Smertebehandling", "beskrivelse...", 1, 0);
INSERT INTO service (name, description, visible_for_visitator, visible_for_emergency_doctor) VALUES ("Tilsyn/observation af borger i dårlig alm tilstand. Medicinsk, psykisk og socialt.", "beskrivelse...", 1, 0);
