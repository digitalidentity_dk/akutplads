ALTER TABLE municipality ADD COLUMN phone_number_from VARCHAR(5) DEFAULT '08:00';
ALTER TABLE municipality ADD COLUMN phone_number_to VARCHAR(5) DEFAULT '17:00';
ALTER TABLE municipality ADD COLUMN phone_number_secondary VARCHAR(255);
UPDATE municipality SET phone_number_secondary = phone_number;
