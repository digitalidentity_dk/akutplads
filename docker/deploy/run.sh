#!/bin/bash

if [ -z "$MAX_HEAP" ]
then
	MAX_HEAP=-Xmx128m
fi

java $MAX_HEAP -jar akutplads.jar
