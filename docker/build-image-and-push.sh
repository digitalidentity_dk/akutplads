#!/bin/bash
docker login telemed-dockerregistry-test.rm.dk

rm -Rf build/
mkdir build

cp -R ../webjar build
cp -R ../ui build
cp -R ../pom.xml build

docker build -t akutplads:latest .
docker tag akutplads:latest telemed-dockerregistry-test.rm.dk/akutplads:$(date +'%Y-%m-%d')
docker push telemed-dockerregistry-test.rm.dk/akutplads:$(date +'%Y-%m-%d')

rm -Rf build/
